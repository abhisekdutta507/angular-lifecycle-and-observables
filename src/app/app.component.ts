import { Component } from '@angular/core';
import { StoreService } from './store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-lifecycles-and-observables';

  amount: number = 0;

  enabled: boolean = true;

  constructor(private store: StoreService) {

  }

  add(data: number) {
    this.amount += data;
    this.store.update({ amount: this.amount });
  }

  ngDoCheck() {
    print('ngDoCheck');
  }

  ngAfterContentChecked() {
    print('ngAfterContentChecked');
  }

  ngAfterViewChecked() {
    print('ngAfterViewChecked');
  }
}

const print = (data: any) => {
  // console.log(data);
};
