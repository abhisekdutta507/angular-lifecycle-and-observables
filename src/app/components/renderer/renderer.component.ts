import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { StoreService, StoreData } from '../../store.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'renderer',
  templateUrl: './renderer.component.html',
  styleUrls: ['./renderer.component.scss']
})
export class RendererComponent implements OnInit {

  @Input() amount: number;
  @Input() awesome: boolean;

  public total: number;

  public unit: number = 3;

  private subscriptions: Subscription[] = [];

  constructor(
    private store: StoreService
  ) {
    print('constructor');
  }

  ngOnInit() {
    print('ngOnInit');

    const sn: Subscription = this.store.observable$.subscribe((store: StoreData) => {
      this.total = store.amount * this.unit;
    });

    this.subscriptions.push(sn);
  }

  /**
   * @description checks are executed whenever component is updated.
   */
  ngDoCheck() {
    print('ngDoCheck');
  }

  ngAfterContentInit() {
    print('ngAfterContentInit');
  }

  /**
   * @description checks are executed whenever component is updated.
   */
  ngAfterContentChecked() {
    print('ngAfterContentChecked');
  }

  ngAfterViewInit() {
    print('ngAfterViewInit');
  }

  /**
   * @description checks are executed whenever component is updated.
   */
  ngAfterViewChecked() {
    print('ngAfterViewChecked');
  }

  ngOnChanges(changes: SimpleChanges) {
    print('ngOnChanges', changes);
  }

  ngOnDestroy() {
    print('ngOnDestroy', this.subscriptions);

    this.subscriptions.forEach((sn: Subscription) => {
      sn.unsubscribe();
    });
  }

}

const print = (...data: any[]) => {
  console.log(...data);
};

/**
 * @description when loaded for 1st time
 * @url https://angular.io/guide/lifecycle-hooks#lifecycle-event-sequence
  constructor
  ngOnChanges
  ngOnInit
  ngDoCheck
  ngAfterContentInit
  ngAfterContentChecked
  ngAfterViewInit
  ngAfterViewChecked

  * @description on component update by props
  ngOnChanges

  * @description on component update by [(ngModel)] and props
  ngDoCheck
  ngAfterContentChecked
  ngAfterViewChecked

  * @description called immediately before Angular destroys the directive or component.
  ngOnDestroy
*/

