import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  private data: StoreData = {
    amount: 0
  };
  private subject = new BehaviorSubject(this.data);
  public observable$: Observable<StoreData> = this.subject.asObservable();

  constructor() { }

  update(newData: StoreData) {
    const data = {
      ...this.data, ...newData
    };
    this.subject.next(data);
  }
}

export interface StoreData {
  amount?: number;
}
