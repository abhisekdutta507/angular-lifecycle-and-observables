This project is created with [Angular CLI](https://angular.io/cli).
We have typescript support in this project using [typescript](https://code.visualstudio.com/docs/typescript/typescript-compiling) with [webpack-cli](https://webpack.js.org/guides/installation/).

Press  `Cmd + Shift + V`  form your Mac keyboard to preview the README.md.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode. Then, visit http://localhost:4200 through a browser window to run the application.

## Learn More

You can learn more in the [The RxJS library](https://angular.io/guide/rx-library#the-rxjs-library).

### Simple Angular BehaviorSubject Setup In An Angular Project

**Initialize the project with `NPM`.** *Make sure you have a stable version of* [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/) & [Angular CLI](https://angular.io/cli) *is installed in your system globally.*

```bash
ng new angular-behavior-subject
```

Answer some questions asked by the `Angular CLI` when initiating the new project.

**Check** the `renderer.component.html` file to understand the RxJS BehaviorSubject. Visit [renderer.component.ts](https://bitbucket.org/abhisekdutta507/angular-lifecycle-and-observables/src/master/src/app/components/renderer/renderer.component.ts).

Please visit [YouTube](https://www.youtube.com/watch?v=-mwNLRbfKmU) for more info.

**Description** - **RxJS** (Reactive Extensions for JavaScript) is a library for reactive programming using observables that makes it easier to compose asynchronous or callback-based code.

```typescript
/*
 * @description Injectable store service
 */
export interface StoreData {
  amount?: number;
}

import { BehaviorSubject, Observable } from 'rxjs';

...

private object: StoreData = {
  amount: 0
};
private subject = new BehaviorSubject(this.object);
public data$: Observable<StoreData> = this.subject.asObservable();

update(newObject: StoreData) {
  const object = {
    ...this.object, ...newObject
  };

  // is used to update the data variable & informs the update to the subscribers
  this.subject.next(object);
}
```

and

```ts
/*
 * @description Component as subscriber
 */
import { Service, StoreData } from './store.service';
import { Subscription } from 'rxjs';

...

public data: StoreData;

private subscriptions: Subscription[] = [];

constructor(private store: Service) {
}

ngOnInit() {
  const sn: Subscription = this.store.data$.subscribe((store: StoreData) => {
    this.data = store;
  });

  this.subscriptions.push(sn);
}

add(data: number) {
  // call the update function with updated data
  this.store.update({ amount: this.data });
}

ngOnDestroy() {
  this.subscriptions.forEach((sn: Subscription) => {
    sn.unsubscribe();
  });
}
```

### Simple Angular Lifecycles

An `Angular Component` has **lifecycles** that starts when Angular instantiates the component class and renders the component view along with its child views. Read more about lifecycle event sequence [here](https://angular.io/guide/lifecycle-hooks#lifecycle-event-sequence).

As per the order of their execution,

- **ngOnChanges**(changes: **SimpleChanges**) - Called before *ngOnInit()* and whenever one or more input props are changed.
```ts
import { SimpleChanges } from '@angular/core';

...

/*
 * @description on component update by props
 */
ngOnChanges(changes: SimpleChanges) {
  // perform the required tasks
}
```
For eg. `SimpleChanges` is
```json
{
  "amount": {
    "currentValue": "number",
    "firstChange": "boolean. true when it is executed for the first time. else is false.",
    "previousValue": "number | undefined"
  }
}
```

- **ngOnInit**() - Called once, after the first *ngOnChanges()*.

- **ngDoCheck**() - Called immediately after *ngOnChanges()* on every change detection run, and immediately after *ngOnInit()* on the first run.

- **ngAfterContentInit**() - Called once, after the first *ngDoCheck()*.
  
- **ngAfterContentChecked**() - Called after the *ngAfterContentInit()* and every subsequent *ngDoCheck()*.
  
- **ngAfterViewInit**() - Called once, after the first *ngAfterContentChecked()*.
  
- **ngAfterViewChecked**() - Called after the *ngAfterViewInit()* and every subsequent *ngAfterContentChecked()*.

- **ngOnDestroy**() - Called immediately before Angular destroys the directive or component.


Hurray!! we have learned the basic concepts of Angular lifecycles & RxJS Behaviour subject.